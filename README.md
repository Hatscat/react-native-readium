# @koober/react-native-readium

react-native glue code for the awesome Readium SDK.

## Installation

```sh
npm install @koober/react-native-readium
```

## Usage

```js
import ReactNativeReadium from "@koober/react-native-readium";

// ...

const deviceName = await ReactNativeReadium.getDeviceName();
```

## License

MIT
