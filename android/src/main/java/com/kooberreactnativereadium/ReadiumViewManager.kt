package com.kooberreactnativereadium

import android.view.View
import com.facebook.react.uimanager.SimpleViewManager
import com.facebook.react.uimanager.ThemedReactContext
import com.facebook.react.views.text.ReactTextView

class ReadiumViewManager : SimpleViewManager<View>() {

  override fun getName() = "ReadiumView"

  override fun createViewInstance(context: ThemedReactContext): View {
    val textView = ReactTextView(context)
    textView.text = "Hello World"

    return textView
  }
}
