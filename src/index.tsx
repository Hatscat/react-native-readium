import { requireNativeComponent, HostComponent, View } from 'react-native';

type ViewProps = React.ComponentProps<typeof View>;
export type RCTReadiumViewProps = ViewProps & {};

const RCTReadiumView: HostComponent<RCTReadiumViewProps> = requireNativeComponent(
  'ReadiumView'
);
export default RCTReadiumView;
