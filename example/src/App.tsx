import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import ReadiumView from '@koober/react-native-readium';

export default function App() {
  return (
    <View style={styles.container}>
      <ReadiumView style={{ height: 300, width: 400 }} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
